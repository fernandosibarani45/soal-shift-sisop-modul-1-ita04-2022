#!/bin/bash

#Buat direktori log
if ! [[ -d "/home/ubuntu/log/" ]]
then
    mkdir "/home/ubuntu/log/"
fi

file_metrics="/home/ubuntu/log/metrics_agg_$(date '+%Y%m%d%H').log"

data=`ls -t /home/ubuntu/log/metrics_2*.log`

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $file_metrics
echo 'minimum,'`awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data | awk -F "," '{ 
                                                                    if (a>$1 || a==0){ a=$1 }
                                                                    if (b>$2 || b==0){ b=$2 }
                                                                    if (c>$3 || c==0){ c=$3 }
                                                                    if (d>$4 || d==0){ d=$4 }
                                                                    if (e>$5 || e==0){ e=$5 }
                                                                    if (f>$6 || f==0){ f=$6 }
                                                                    if (g>$7 || g==0){ g=$7 }
                                                                    if (h>$8 || h==0){ h=$8 }
                                                                    if (i>$9 || i==0){ i=$9 }
                                                                    if (k>$1 || k==0){k=$11}
                                                                    } END{print a","b","c","d","e","f","g","h","i","$10","k}'` >> $file_metrics
echo 'maksimum,'`awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data | awk -F "," '{ 
                                                                    if (a<$1){ a=$1 }
                                                                    if (b<$2){ b=$2 }
                                                                    if (c<$3){ c=$3 }
                                                                    if (d<$4){ d=$4 }
                                                                    if (e<$5){ e=$5 }
                                                                    if (f<$6){ f=$6 }
                                                                    if (g<$7){ g=$7 }
                                                                    if (h<$8){ h=$8 }
                                                                    if (i<$9){ i=$9 }
                                                                    if (k<$11){k=$11}
                                                                    } END{print a","b","c","d","e","f","g","h","i","$10","k}'` >> $file_metrics
echo 'average,'`awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data |  awk -F "," '{a+=$1;b+=$2;c+=$3;d+=$4;e+=$5;f+=$6;g+=$7;h+=$8;i+=$9;j=$10;k+=$11;n++} END{print a/n","b/n","c/n","d/n","e/n","f/n","g/n","h/n","i/n","j","k/n}'` >> $file_metrics

chmod 0700 $file_metrics