# a. Membuat direktori
if ! [[ -d "./forensic_log_website_daffainfo_log" ]]
then
   mkdir "./forensic_log_website_daffainfo_log"
fi

# b. Rata-rata request/jam
line=`awk 'END {print NR}' log_website_daffainfo.log` #total line pada log | output : 974
request=`expr $line - 1` #line 1 bukan request | output : 973
start=`awk -F"[ :]" 'NR == 2 {print $3}' log_website_daffainfo.log` #request pertama dikirim jam 00
end=`awk -F"[ :]" 'END {print $3}' log_website_daffainfo.log` #request terakhir dikirim jam 12
time=`expr $end - $start` #lama waktu request dikirim | output 13
mean=`expr $request / $time` #rata-rata request/jam
printf "Rata-rata serangan adalah sebanyak $mean requests per jam" > ./forensic_log_website_daffainfo_log/ratarata.txt

# c. IP yang paling banyak melakukan request
ip_req=`awk -F"[\"]" 'NR>1{
    ip_count[$2]++
    }
    END{
        for (ip in ip_count){
            if (max < ip_count[ip]){
                max = ip_count[ip]
                max_ip = ip
            }
        }
        printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests", max_ip, ip_count[max_ip];
    }' log_website_daffainfo.log`
printf "$ip_req\n\n" > ./forensic_log_website_daffainfo_log/result.txt

# d. Banyak requests yang menggunakan user-agent curl
curl=`awk -F"[\"]" '/curl/ {
    i++
    }
    END{
        print i
    }' log_website_daffainfo.log`
printf "Ada $curl requests yang menggunakan curl sebagai user-agent\n\n" >> ./forensic_log_website_daffainfo_log/result.txt

# e. IP yang mengakses website pada jam 2 pagi
jam2=`awk -F"[\"]" '/2022:02/ {
    ip_count[$2]++
    }
    END{
        for (ip in ip_count)
            print ip
    }' log_website_daffainfo.log`
printf "$jam2" >> ./forensic_log_website_daffainfo_log/result.txt