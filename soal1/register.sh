#!/bin/bash

# Buat direktori users.txt
if ! [[ -d "users" ]]
then
    mkdir "users"
fi

user_file="users/user.txt"

#Check apakah file data user sudah ada
if ! [ -f "$user_file" ]
then
    touch "$user_file"
fi

store () {
    if grep -q "${1,,} " $user_file
    then
        echo  "$(date '+%Y-%m-%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
        echo "Username telah terdaftar"
    else
        echo  "$(date '+%Y-%m-%d %H:%M:%S') REGISTER: INFO User ${username,,} registered successfully" >> log.txt
        echo ${1,,} $2 >> $user_file
        echo "Registrasi Berhasil"
    fi
}

# input dan validasi data
while true
do
    berhasil=true
    read -p 'Username: ' username
    read -sp 'Password: ' pass
    echo
    if [ ${#pass} -lt 8 ]
    then
        berhasil=false
        echo "Password minimal 8 karakter"
    fi

    if ! (echo "$pass" | grep -q "[A-Z]") && (echo "$pass" | grep -q "[a-z]") 
    then
        berhasil=false
        echo "Password memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    fi
    
    if ! (echo "$pass" | grep -q "[0-9 ]" )
    then
        berhasil=false
        echo "Password harus alphanumeric"
    fi

    if [ "$username" == "$pass" ]
    then
        berhasil=false
        echo "Password dan Username tidak boleh sama"
    fi

    if $berhasil
    then
        store ${username,,} $pass
        break
    else
        echo
    fi
done
