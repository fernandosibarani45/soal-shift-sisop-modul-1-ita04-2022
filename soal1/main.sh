#!/bin/bash

# Buat direktori users
if ! [[ -d "users" ]]
then
    mkdir "users"
fi

user_file="users/user.txt"
#Check apakah file data user sudah ada
if ! [ -f "$user_file" ]
then
    touch "$user_file"
fi

read -p 'Username: ' username
read -sp 'Password: ' password
echo

dl () {
    #Check apakah zip sudah ada
    if [ -f "$(date '+%Y-%m-%d')_${username,,}.zip" ]
    then
        echo "$(date '+%Y-%m-%d')_${username,,}.zip"
        unzip -P $password "$(date '+%Y-%m-%d')_${username,,}.zip"
        rm "$(date '+%Y-%m-%d')_${username,,}.zip"
    fi

    # Buat direktori download file
    if ! [[ -d "$(date '+%Y-%m-%d')_${username,,}" ]]
    then
        file_number=0
        mkdir "$(date '+%Y-%m-%d')_${username,,}"
    else
        file_number=1
    fi
    #Dapat jumlah file jpg terakhir
    for file in $(date '+%Y-%m-%d')_${username,,}/*.jpg
    do
        file_number=$((file_number+1))
    done
    
    # Bikin File Baru
    for ((num=1; num<=$1; num=num+1))
    do
        if [[ $((10#$file_number)) -lt $((10#10)) ]]
        then
            file_number="0${file_number}"
        else
            file_number=${file_number}
        fi
        wget -O "$(date '+%Y-%m-%d')_${username,,}/PIC_$file_number.jpg" "https://loremflickr.com/320/240"
        file_number=${file_number#0}
        file_number=$((file_number + 1))
    done

    # Zip File
    zip --password $password "$(date '+%Y-%m-%d')_${username,,}.zip" $(date '+%Y-%m-%d')_${username,,}/*
    rm -r $(date '+%Y-%m-%d')_${username,,}
}

att () {
    echo "Login baik : " `awk '/LOGIN: INFO User '"$username"' logged in/ { i++ } END{ print i}' log.txt`
    echo "Login tidak baik : " `awk '/LOGIN: ERROR Failed login attempt on user '"$username"'/ { i++ } END{ print i }' log.txt`
}

#Check Username
if grep -q "${username,,} " $user_file
then
    #Check Password
	if grep -Fxq "${username,,} $password" $user_file
    then
        echo  "$(date '+%Y-%m-%d %H:%M:%S')  LOGIN: INFO User ${username,,} logged in" >> log.txt
        echo "Login Berhasil"
        while true
        do
            read perintah
            $perintah
            echo
        done
    else
        echo  "$(date '+%Y-%m-%d %H:%M:%S')  LOGIN: ERROR Failed login attempt on user ${username,,}" >> log.txt
        echo "Username / Password Salah"
    fi
else
	echo "Username Tidak Terdaftar"
fi